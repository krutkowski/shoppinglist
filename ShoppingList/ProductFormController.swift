
import SDK
import UIKit

class ProductFormController: UITableViewController {

	let productsManager = ProductsManager()
	@IBOutlet private var nameField: UITextField!
	@IBOutlet private var noteField: UITextField!
	@IBOutlet private var inventoryField: UITextField!

	@IBAction private func userDidPressSaveButton() {
		// TODO: error thinking for each unwrapping and adding product
		let product = Product(name: nameField.text!)
		product.inventory = Int(inventoryField.text!)!

		try! productsManager.addProduct(product)
	}
}
