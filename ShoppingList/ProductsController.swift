
import SDK
import UIKit

class ProductsController: UITableViewController {

	let productsManager = ProductsManager()
	private lazy var allProductsList: ProductsList = {
		return self.productsManager.allProductsList()
	}()

	private var currentList: ProductsList {
		return allProductsList
	}

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	// MARK: table protocol

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return currentList.products.count
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ProductCell

		let product = allProductsList.products[indexPath.row]
		cell.nameLabel.text = product.name

		return cell
	}
}
