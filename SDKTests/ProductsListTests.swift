
import SDK
import XCTest

class ProductsListTests: XCTestCase {

	let productsManager = ProductsManager()

	override func setUp() {
		super.setUp()

		productsManager.removeAllProducts()
	}

	func testAddingItem() {
		let productsList = productsManager.allProductsList()

		var receivedWillCallback = false
		productsList.willChangeProducts = {
			receivedWillCallback = true
		}

		var receivedDidCallback = false
		productsList.didChangeProducts = {
			receivedDidCallback = true
		}

		let product = Product(name: "A product")
		try! productsManager.addProduct(product)

		XCTAssertEqual(productsList.products[0].name, "A product")
		XCTAssertTrue(receivedWillCallback)
		XCTAssertTrue(receivedDidCallback)
	}

//	func testAddingToMissingList_addMissingProduct() {
//		let productsList = productsManager.missingProducts()
//
//		var receivedCallback = false
//		productsList.willChangeProducts = {
//			receivedCallback = true
//		}
//
//		let product = Product(name: "")
//		product.desiredQuantity = 1
//		product.haveQuantity = 0
//		try! productsManager.addProduct(product)
//
//		XCTAssertTrue(receivedCallback)
//	}
//
//	func testAddingToMissingList_addHaveProduct() {
//		let productsList = productsManager.missingProducts()
//
//		var receivedCallback = false
//		productsList.willChangeProducts = {
//			receivedCallback = true
//		}
//
//		let product = Product(name: "")
//		product.desiredQuantity = 1
//		product.haveQuantity = 1
//		try! productsManager.addProduct(product)
//
//		XCTAssertFalse(receivedCallback)
//	}
}
