
import SDK
import XCTest

class ProductsManagerTests: XCTestCase {
	let manager = ProductsManager()

    override func setUp() {
        super.setUp()

		manager.removeAllProducts()
    }

	func testListingProducts() {
		XCTAssertEqual(manager.allProducts().count, 0)
	}

	func testAddingProducts() {
		let product = Product(name: "TestProduct")
		product.inventory = 3
		try! manager.addProduct(product)

		let allProducts = manager.allProducts()
		XCTAssertEqual(allProducts.count, 1)

		XCTAssertEqual(allProducts[0].name, "TestProduct")
		XCTAssertEqual(allProducts[0].inventory, 3)
	}

	func testAddingItemTwice() {
		let product = Product(name: "")
		try! manager.addProduct(product)

		var receivedException = false
		do {
			try manager.addProduct(product)
		} catch Error.ModelAlreadyAdded {
			receivedException = true
		} catch {

		}

		XCTAssertTrue(receivedException)
	}

	func testUpdatingProduct() {
		let product = Product(name: "TestProduct")
		product.inventory = 3
		try! manager.addProduct(product)

		product.name = "New name"
		product.inventory = 4
		try! manager.updateProduct(product)

		let allProducts = manager.allProducts()
		XCTAssertEqual(allProducts.count, 1)

		XCTAssertEqual(allProducts[0].name, "New name")
		XCTAssertEqual(allProducts[0].inventory, 4)
	}

	func testUpdatingNotAddedProduct() {
		let product = Product(name: "")

		var receivedException = false
		do {
			try manager.updateProduct(product)
		} catch Error.ModelHasToBeAddedFirst {
			receivedException = true
		} catch {

		}

		XCTAssertTrue(receivedException)
	}
}
