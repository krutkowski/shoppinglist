
import SDK
import XCTest

class MealsManagerTests: XCTestCase {

	let manager = MealsManager()

	override func setUp() {
		super.setUp()

		manager.removeAllMeals()
	}

	func testLoadingAllMealsBeforeAddingAnyMeal() {
		let meals = manager.allMeals()
		XCTAssertEqual(meals.count, 0)
	}

	func testLoadingMeal() {
		let meal = manager.mealForWeekday(.Tuesday, type: .Lunch)
		XCTAssertEqual(meal.weekday, Weekday.Tuesday)
		XCTAssertEqual(meal.type, MealType.Lunch)

		let meals = manager.allMeals()
		XCTAssertEqual(meals.count, 1)
	}

	func testLoadingSameMealTwice() {
		let meal1 = manager.mealForWeekday(.Tuesday, type: .Lunch)
		XCTAssertEqual(meal1.weekday, Weekday.Tuesday)
		XCTAssertEqual(meal1.type, MealType.Lunch)

		let meal2 = manager.mealForWeekday(.Tuesday, type: .Lunch)
		XCTAssertEqual(meal2.weekday, Weekday.Tuesday)
		XCTAssertEqual(meal2.type, MealType.Lunch)

		let meals = manager.allMeals()
		XCTAssertEqual(meals.count, 1)
	}
}
