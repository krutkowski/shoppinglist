
import Foundation

public enum MealType: Int {
	case Breakfast = 0
	case Breakfast2 = 1
	case Lunch = 2
	case Dinner = 3
	case Supper = 4
}
