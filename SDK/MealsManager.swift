
import Foundation

public class MealsManager {
	private let persistence = Persistence.sharedInstance

	public init() {

	}

	public func allMeals() -> [Meal] {
		do {
			return try persistence.allMeals()
		} catch {

		}

		return []
	}

	public func removeAllMeals() {
		do {
			try persistence.removeAllMeals()
		} catch {

		}
	}

	public func mealForWeekday(weekday: Weekday, type: MealType) -> Meal {

		do {
			let existingMeal = try persistence.mealForWeekday(weekday, type: type)

			if let existingMeal = existingMeal {
				return existingMeal
			}
		} catch {

		}

		let meal = Meal(weekday: weekday, type: type)

		do {
			try persistence.addMeal(meal)
		} catch {

		}

		return meal
	}
}