
import Foundation

public class Product {

	public var name: String
	public var inventory: Int = 0
	var productCD: ProductCD?

	public init(name: String) {
		self.name = name
	}

	init(productCD: ProductCD) {
		self.productCD = productCD
		name = productCD.name
		inventory = Int(productCD.inventory)
	}
}
