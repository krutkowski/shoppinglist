
import Foundation

public class ProductsManager {
	private let persistence = Persistence.sharedInstance

	public func allProducts() -> [Product] {
		do {
			return try persistence.allProducts()
		} catch {

		}

		return []
	}

	public init() {

	}

	public func addProduct(product: Product) throws {
		try persistence.addProduct(product)
	}

	public func updateProduct(product: Product) throws {
		try persistence.updateProduct(product)
	}

	public func removeAllProducts() {
		do {
			try persistence.removeAllProducts()
		} catch {

		}
	}

	public func allProductsList() -> ProductsList {
		let frc = persistence.allProductsFRC()

		return ProductsList(frc: frc)
	}

	public func missingProducts() -> ProductsList {
		let frc = persistence.missingProductsFRC()

		return ProductsList(frc: frc)
	}
}
