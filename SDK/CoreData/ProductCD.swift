
import CoreData
import Foundation

class ProductCD: NSManagedObject {

	@NSManaged var inventory: Int32
	@NSManaged var imageName: String
	@NSManaged var name: String
	@NSManaged var order: Int32
	@NSManaged var meals: Set<MealCD>

	func syncPropertiesWithProduct(product: Product) {
		name = product.name
		inventory = Int32(product.inventory)
	}

	func addMeal(meal: MealCD) {
		let meals = mutableSetValueForKey("meals")
		meals.addObject(meal)
	}

	func removeMeal(meal: MealCD) {
		let meals = mutableSetValueForKey("meals")
		meals.removeObject(meal)
	}
}
