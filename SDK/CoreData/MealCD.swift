
import CoreData
import Foundation

class MealCD: NSManagedObject {

	@NSManaged var weekday: Int16
	@NSManaged var type: Int16
	@NSManaged var products: Set<ProductCD>

	func syncPropertiesWithMeal(meal: Meal) {
		weekday = Int16(meal.weekday.rawValue)
		type = Int16(meal.type.rawValue)
	}

	func addProduct(product: ProductCD) {
		let products = mutableSetValueForKey("products")
		products.addObject(product)
	}

	func removeProduct(product: ProductCD) {
		let products = mutableSetValueForKey("products")
		products.removeObject(product)
	}
}
