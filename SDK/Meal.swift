
import Foundation

public class Meal {

	public var weekday: Weekday
	public var type: MealType
	var mealCD: MealCD?

	init(mealCD: MealCD) {
		self.mealCD = mealCD
		self.weekday = Weekday(rawValue: Int(mealCD.weekday))!
		self.type = MealType(rawValue: Int(mealCD.type))!
	}

	init(weekday: Weekday, type: MealType) {
		self.weekday = weekday
		self.type = type
	}
}
