
import Foundation
import UIKit

extension NSURL
{
	public class func appDocumentsDirectory() -> NSURL {
		let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)

		return urls.last!
	}
}
