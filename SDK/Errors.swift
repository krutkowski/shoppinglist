
import Foundation

public enum Error: ErrorType {
	case GenericDatabase
	case ModelHasToBeAddedFirst
	case ModelAlreadyAdded
}
