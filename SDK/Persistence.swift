
import CoreData
import Foundation

// TODO: Product as static, same with properties like name

class Persistence {

	private init() {
		removePersistentStore()
	}

	static let sharedInstance = Persistence()

	// MARK: products

	func addProduct(product: Product) throws {
		guard product.productCD == nil else {
			throw Error.ModelAlreadyAdded
		}

		let productCD = NSEntityDescription.insertNewObjectForEntityForName("Product", inManagedObjectContext: context) as! ProductCD
		productCD.syncPropertiesWithProduct(product)
		product.productCD = productCD

		do {
			try context.save()
		} catch {
			throw Error.GenericDatabase
		}
	}

	func updateProduct(product: Product) throws {
		guard let productCD = product.productCD else {
			throw Error.ModelHasToBeAddedFirst
		}

		productCD.syncPropertiesWithProduct(product)

		do {
			try context.save()
		} catch {
			throw Error.GenericDatabase
		}
	}

	func allProductsFRC() -> NSFetchedResultsController {
		let request = NSFetchRequest(entityName: "Product")
		request.sortDescriptors = [NSSortDescriptor(key: "order", ascending: true)]

		return NSFetchedResultsController(
			fetchRequest: request,
			managedObjectContext: context,
			sectionNameKeyPath: nil,
			cacheName: nil)
	}

	func missingProductsFRC() -> NSFetchedResultsController {
		let request = NSFetchRequest(entityName: "Product")
		request.sortDescriptors = [NSSortDescriptor(key: "order", ascending: true)]
		request.predicate = NSPredicate(format: "desiredQuantity > haveQuantity")

		return NSFetchedResultsController(
			fetchRequest: request,
			managedObjectContext: context,
			sectionNameKeyPath: nil,
			cacheName: nil)
	}

	func allProducts() throws -> [Product] {
		let request = NSFetchRequest(entityName: "Product")
		request.sortDescriptors = [NSSortDescriptor(key: "order", ascending: true)]

		do {
			let productCDs = try context.executeFetchRequest(request) as! [ProductCD]

			var products = [Product]()
			for productCD in productCDs {
				let product = Product(productCD: productCD)
				products.append(product)
			}

			return products
		} catch {
			throw Error.GenericDatabase
		}
	}

	func removeAllProducts() throws {
		let request = NSFetchRequest(entityName: "Product")
		let productCDs = try context.executeFetchRequest(request) as! [ProductCD]

		for productCD in productCDs {
			context.deleteObject(productCD)
		}

		do {
			try context.save()
		} catch {
			throw Error.GenericDatabase
		}
	}

	// MARK: meals

	func addMeal(meal: Meal) throws {
		guard meal.mealCD == nil else {
			throw Error.ModelAlreadyAdded
		}

		let mealCD = NSEntityDescription.insertNewObjectForEntityForName("Meal", inManagedObjectContext: context) as! MealCD
		mealCD.syncPropertiesWithMeal(meal)
		meal.mealCD = mealCD

		do {
			try context.save()
		} catch {
			throw Error.GenericDatabase
		}
	}

	func mealForWeekday(weekday: Weekday, type: MealType) throws -> Meal? {
		let request = NSFetchRequest(entityName: "Meal")
		let weekdayInt16 = Int16(weekday.rawValue)
		let typeInt16 = Int16(type.rawValue)
		request.predicate = NSPredicate(format: "weekday == \(weekdayInt16) && type == \(typeInt16)")
		request.fetchLimit = 1

		do {
			let mealCDs = try context.executeFetchRequest(request) as! [MealCD]


			if mealCDs.count == 1 {
				let mealCD = mealCDs[0]
				return Meal(mealCD: mealCD)
			} else {
				return nil
			}
		} catch {
			throw Error.GenericDatabase
		}
	}

	func allMeals() throws -> [Meal] {
		let request = NSFetchRequest(entityName: "Meal")
		request.sortDescriptors = [
			NSSortDescriptor(key: "weekday", ascending: true),
			NSSortDescriptor(key: "type", ascending: true)
		]

		do {
			let mealCDs = try context.executeFetchRequest(request) as! [MealCD]

			var meals = [Meal]()
			for mealCD in mealCDs {
				let meal = Meal(mealCD: mealCD)
				meals.append(meal)
			}

			return meals
		} catch {
			throw Error.GenericDatabase
		}
	}

	func removeAllMeals() throws {
		let request = NSFetchRequest(entityName: "Meal")
		let mealCDs = try context.executeFetchRequest(request) as! [MealCD]

		for mealCD in mealCDs {
			context.deleteObject(mealCD)
		}

		do {
			try context.save()
		} catch {
			throw Error.GenericDatabase
		}
	}

	// MARK: other

	private func storeURL() -> NSURL {
		return NSURL.appDocumentsDirectory().URLByAppendingPathComponent("DataModel.sqlite")
	}

	func removePersistentStore() {
		let url = storeURL()

		do {
			try NSFileManager.defaultManager().removeItemAtURL(url)
		} catch {

		}

	}

	// MARK: - Core data

	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
		let bundle = NSBundle(forClass: object_getClass(self))
		let modelURL = bundle.URLForResource("DataModel", withExtension: "momd")!
		let model = NSManagedObjectModel(contentsOfURL: modelURL)!

		var coordinator: NSPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
		let url = NSURL.appDocumentsDirectory().URLByAppendingPathComponent("DataModel.sqlite")


		do {
			try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
		} catch {
		}

		return coordinator
		}()

	lazy var context: NSManagedObjectContext = {
		var context = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
		context.persistentStoreCoordinator = self.persistentStoreCoordinator

		return context
		}()
}
