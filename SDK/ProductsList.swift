
import CoreData
import Foundation

// TODO: hide protocol adoption, don't inherit from NSObject, remove public from delegate methods

public class ProductsList: NSObject, NSFetchedResultsControllerDelegate {

	private let frc: NSFetchedResultsController
	public var products: [Product] {
		var products: [Product] = []

		let productCDs = frc.fetchedObjects as! [ProductCD]
		for productCD in productCDs {
			let product = Product(productCD: productCD)
			products.append(product)
		}

		return products
	}

	public typealias EmptyClosure = Void -> Void

	public var willChangeProducts: EmptyClosure?
	public var didChangeProducts: EmptyClosure?

	init(frc: NSFetchedResultsController) {
		self.frc = frc

		try! frc.performFetch()

		super.init()

		self.frc.delegate = self
	}

	public func controllerWillChangeContent(controller: NSFetchedResultsController) {
		willChangeProducts?()
	}

	public func controllerDidChangeContent(controller: NSFetchedResultsController) {
		didChangeProducts?()
	}

	public func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
		
	}
}
